
/* ANIMATION MENU BURGER => DROP MENU */

var iPad = window.matchMedia("(max-width: 768px)")
iPadFunction(iPad) // Call listener function at run time
iPad.addListener(iPadFunction) // Attach listener function on state changes

function iPadFunction(iPad) {
    if (iPad.matches) { // If media query matches
        $("#burger").css("display", "block");
        $(".item").css("display", "none");
        $("#burger").on('click', function() {
            $(this).css("display", "none");
            
            $("#cross").css("display", "block");

            $(".item").addClass("item2");
            $(".item2").css("display", "block");
            
            $("a.nav-item").css("font-size", "4vw");

        });
        $("#cross").on('click', function() {
            $(this).css("display", "none");
            
            $("#burger").css("display", "block");

            $(".item").css("display", "none");
        });
    }
    else {
        $("#cross").css("display", "none");
        $("#burger").css("display", "none");
        $(".item").removeClass("item2");
        $(".item").css("display", "block");
        $("a.nav-item").css("font-size", "2vw");
    }
}

/*---------------------------SLIDE HISTOIRE-------------------------------*/ 
/*---------------------------SLIDE HISTOIRE-------------------------------*/ 
/*---------------------------SLIDE HISTOIRE-------------------------------*/ 

var nbClicks = 0;
var slideIndex = [1,1];
var slideId = ["mySlides1"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
    showSlides(slideIndex[no] += n, no);
    changeBackground();
}

function showSlides(n, no) {
    var i;
    var x = document.getElementsByClassName(slideId[no]);
    if (n > x.length) {slideIndex[no] = 1}    
    if (n < 1) {slideIndex[no] = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    x[slideIndex[no]-1].style.display = "block";  
}

function changeBackground() {
    nbClicks++;
    if (nbClicks % 2 == 0) {
        $("body").css("background-image", "url('../images/Notre-Histoire-01.jpg')");
    } else {
        $("body").css("background-image", "url('../images/Notre-Histoire-02.jpg')");
}
}
        
/*---------------------------SLIDE HISTOIRE-------------------------------*/ 
/*---------------------------SLIDE HISTOIRE-------------------------------*/ 
/*---------------------------SLIDE HISTOIRE-------------------------------*/




